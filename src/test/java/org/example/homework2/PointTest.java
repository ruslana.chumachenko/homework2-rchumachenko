package org.example.homework2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PointTest {

  private final static double X = 0.0;
  private final static double Y = 0.0;
  private final static Point POINT_COORDINATES = new Point(10.555555, 0.555555);
  private final static Point POINT_COORDINATES_NEGATIVE = new Point(-15.755555, -0.1255555);

  @Test
  void shouldCalculateDistance() {
    Point point = new Point(X, Y);
    Assert.assertEquals(10.570164744035, point.distance(POINT_COORDINATES), 0.0000000001);
  }

  @Test
  void shouldCalculateDistanceWithNegativeValues() {
    Point point = new Point(X, Y);
    Assert.assertEquals(15.756055265884, point.distance(POINT_COORDINATES_NEGATIVE), 0.0000000001);
  }
}