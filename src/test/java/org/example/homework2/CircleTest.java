package org.example.homework2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {
  private final static double RADIUS = 50.5;
  private final static Point CIRCLE_CENTER = new Point(0.0, 0.0);
  private final static Point POINT_IN_COORDINATES = new Point(10.98763535, 11.24234235);
  private final static Point POINT_OUT_COORDINATES = new Point(88.98763535, 66.24234235);

  @Test
  void shouldReturnTrueIfPointIsInCircle() {

    Circle circle = new Circle(RADIUS, CIRCLE_CENTER);
    assertTrue(circle.isPointInCircle(POINT_IN_COORDINATES));
  }

  @Test
  void shouldReturnFalseIfPointIsOutCircle() {

    Circle circle = new Circle(RADIUS, CIRCLE_CENTER);
    assertFalse(circle.isPointInCircle(POINT_OUT_COORDINATES));
  }
}