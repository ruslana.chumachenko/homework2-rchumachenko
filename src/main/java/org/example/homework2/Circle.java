package org.example.homework2;

public class Circle {
  private final Double radius;
  private final Point circleCenter;

  public Circle(Double radius, Point circleCenter) {
    this.radius = radius;
    this.circleCenter = circleCenter;
  }

  public boolean isPointInCircle(Point point) {
    return point.distance(circleCenter) < radius;
  }


}
