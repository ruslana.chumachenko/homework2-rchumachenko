package org.example.homework2;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class UserCommunication {
  private final Scanner scanner;
  private final PrintStream out;

  public UserCommunication() {
    this.scanner = new Scanner(System.in);
    this.out = System.out;
  }

  public ArrayList readPointsList(int pointsNumber) {
    ArrayList<Point> pointsList = new ArrayList();
    out.println("Please enter coordinates of " + pointsNumber + " points in format: x, y");
    for (int i = 0; i < pointsNumber; i++) {
      String input = scanner.nextLine();
      String[] inputSubstrings = input.split(",");
      Point point = new Point(Double.parseDouble(inputSubstrings[0]), Double.parseDouble(inputSubstrings[1]));
      pointsList.add(point);
    }

    return pointsList;
  }

  public Point readCircleCenter() {
    out.println("Please enter circle center coordinates in format: x, y ");
    String input = scanner.nextLine();
    String[] inputSubstrings = input.split(",");
    Point circleCenter = new Point(Double.parseDouble(inputSubstrings[0]), Double.parseDouble(inputSubstrings[1]));
    return circleCenter;
  }

  public Double readCircleRadius() {
    out.println("Please enter circle radius: ");
    Double circleRadius = scanner.nextDouble();
    scanner.nextLine();
    return circleRadius;
  }

  public void printGameResults(ArrayList<Point> pointsInCircle) {
    out.println("Points in circle: " + pointsInCircle);
  }
}
