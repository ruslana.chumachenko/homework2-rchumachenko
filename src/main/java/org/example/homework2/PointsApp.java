package org.example.homework2;

import java.util.ArrayList;

public class PointsApp {

  private UserCommunication uc;
  private Circle circle;

  public PointsApp() {
    this.uc = new UserCommunication();
    this.circle = new Circle(uc.readCircleRadius(), uc.readCircleCenter());
  }

  public ArrayList<Point> getPointsInCircle(ArrayList<Point> pointsList) {
    ArrayList<Point> pointsInCircleList = new ArrayList<>();
    for (int i = 0; i < pointsList.size(); i++) {
      if (circle.isPointInCircle(pointsList.get(i))) {
        pointsInCircleList.add(pointsList.get(i));
      }
    }
    return pointsInCircleList;
  }
}
