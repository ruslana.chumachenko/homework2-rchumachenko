package org.example.homework2;

import java.util.ArrayList;

public class Main {

  public static void main(String[] args) {

    UserCommunication uc = new UserCommunication();
    PointsApp pointsApp = new PointsApp();
    ArrayList<Point> pointsList = uc.readPointsList(10);
    ArrayList<Point> pointsInCircle = pointsApp.getPointsInCircle(pointsList);
    uc.printGameResults(pointsInCircle);
  }

}
